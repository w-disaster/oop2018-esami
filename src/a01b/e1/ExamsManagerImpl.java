package a01b.e1;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExamsManagerImpl implements ExamsManager {

	private Map<String, Map<String, ExamResult>> manager;
	
	public ExamsManagerImpl() {
		this.manager = new HashMap<>();
	}

	@Override
	public void createNewCall(String call) {
		if(this.manager.containsKey(call)) {
			throw new IllegalArgumentException();
		}
		this.manager.put(call, new HashMap<>());
	}

	@Override
	public void addStudentResult(String call, String student, ExamResult result) {
		Map<String, ExamResult> cexams = this.manager.get(call);
		if(cexams.containsKey(student)) {
			throw new IllegalArgumentException();
		}
		cexams.put(student, result);
	}

	@Override
	public Set<String> getAllStudentsFromCall(String call) {
		return this.manager.get(call).keySet();
	}

	@Override
	public Map<String, Integer> getEvaluationsMapFromCall(String call) {
		return this.manager.get(call)
				.entrySet()
				.stream()
				.filter(e -> e.getValue().getEvaluation().isPresent())
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getEvaluation().get()));
	}
	@Override
	public Map<String, String> getResultsMapFromStudent(String student) {
		return this.manager.entrySet()
				.stream()
				.filter(e -> e.getValue().containsKey(student))
				.collect(Collectors.toMap(m -> m.getKey(), m -> m.getValue().get(student).toString()));
	}

	@Override
	public Optional<Integer> getBestResultFromStudent(String student) {
		return this.manager.entrySet()
				.stream()
				.filter(e -> e.getValue().containsKey(student))
				.filter(e -> e.getValue().get(student).getEvaluation().isPresent())
				.map(e -> e.getValue().get(student).getEvaluation().get())
				.max((o1, o2) -> o1 - o2);
	}

}
