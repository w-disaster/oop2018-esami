package a03a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
    public static final int LIMIT = 6;
    private Map<Pair<Integer, Integer>, JButton> bMap;
    private Logics logics;
    
    public GUI() {
    	this.setSize(200,200);
        
        JPanel panel = new JPanel(new GridLayout(6,6));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        this.logics = new LogicsImpl();
        this.bMap= new HashMap<>();
        for(int i = 0; i < LIMIT; i++) {
        	for(int k = 0; k < LIMIT; k++) {
        		JButton nb = new JButton("0");
        		bMap.put(new Pair<>(i,k), nb);
        		panel.add(nb);
        	}
        }
        
        bMap.entrySet().forEach(e -> {
        	e.getValue().addActionListener(l -> {
        		/* we pushed the button */
        		logics.hit(e.getKey());
        		this.updateButtons();
        	});
        });
        
        
        this.add(panel);
        this.setVisible(true);
    }
    
    void updateButtons() {
    	this.bMap.entrySet().forEach(e -> {

    		if(e.getValue().isEnabled()) {
    			e.getValue().setText(logics.getValue(e.getKey()).toString());
    		}
    		e.getValue().setEnabled(logics.isValueEditable(e.getKey()));
    		if(logics.gameEnded()) {
    			System.exit(0);
    		}
    	});
    }
        
}
