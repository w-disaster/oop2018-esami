package a04.e2;

public interface Logics {

	/**
	 * @param key
	 * @return true if the button must set disabled, false otherwise
	 */
	boolean hit(Pair<Integer, Integer> key);
	
	/**
	 * @param key
	 * @return true if the key is adjacent to reference, false otherwise
	 */
	boolean isAdjacent(Pair<Integer, Integer> reference, Pair<Integer, Integer> key);
	
	/**
	 * @param key : the last key hit
	 * @return true if the game is over, false otherwise
	 */
	boolean end(Pair<Integer, Integer> key);
	
}
