package a01b.e2;

import java.util.HashMap;
import java.util.Map;

public class LogicsImpl implements Logics{
	
	private Pair<Boolean, Pair<Integer, Integer>> bishop;
	private Map<Pair<Integer, Integer>, Boolean> grid;
	private static final int LIMIT = 5;
	

	public LogicsImpl() {
		this.bishop = new Pair<>(false, new Pair<>(0,0)); 
		this.grid = new HashMap<>();
		for(int i = 0; i < LIMIT; i++) {
			for(int k = 0; k < LIMIT; k++) {
				this.grid.put(new Pair<>(i, k), true);
			}
		}
	}

	@Override
	public boolean isBishopPosition(Pair<Integer, Integer> pos) {
		return this.bishop.getY().getX() == pos.getX() && 
				this.bishop.getY().getY() == pos.getY();
	}

	@Override
	public void disableNonDiagonal(Pair<Integer, Integer> pos) {
		for(int x = 0; x < LIMIT; x++) {
			for(int y = 0; y < LIMIT; y++) {
				if(Math.abs(pos.getX() - x) != Math.abs(pos.getY() - y)) {
					this.grid.replace(new Pair<>(x, y), false);
				}
			}
		}
	}

	@Override
	public boolean isBishopMoving() {
		return this.bishop.getX();
	}

	@Override
	public boolean gameTerminated() {
		return this.bishop.getY().equals(new Pair<>(0,0));
	}

	@Override
	public Map<Pair<Integer, Integer>, Boolean> getGrid() {
		return this.grid;
	}

	@Override
	public void setBishopState(boolean state) {
		this.bishop.setX(state);
		if(!state) {
			this.grid.entrySet().forEach(e -> e.setValue(true));
		}
	}

	@Override
	public void setBishopPosition(Pair<Integer, Integer> pos) {
		this.bishop.setY(pos);
	}

	
	
}
