package a03b.e2;

import java.util.HashMap;
import java.util.Map;

import javax.swing.*;


public class GUI extends JFrame {
    
	private Map<Integer, JButton> buttons;
	private JCheckBox cb;
	private Logics logics;
	public static final Integer LIMIT = 5;
	
    public GUI() {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	this.setSize(500, 200);
    	
    	this.logics = new LogicsImpl();
    	this.buttons = new HashMap<>();
    	this.cb = new JCheckBox(logics.getDirection().equals(false) ? "right" : "left");
    	JPanel panel = new JPanel();
    	
    	
    	for(int i = 0; i < 5; i++) {
    		JButton nb = new JButton(logics.getValueAt(i).toString());
    		this.buttons.put(i, nb);
    		panel.add(nb);
    	}
    	
    	this.buttons.entrySet().forEach(e -> {
    		e.getValue().addActionListener(l -> {
    			/* a button has been pushed */
    			logics.hit(e.getKey());
    			this.updateBText(e.getKey(), e.getKey() + (logics.getDirection() ? -1 : 1));
    			if(logics.areAscendingOrdered()) {
    				System.exit(0);
    			}
    		});
    	});
    	
    	panel.add(cb);
    	cb.addActionListener(l -> {
    		logics.switchDirection();
    		cb.setText(logics.getDirection().equals(false) ? "right" : "left");
    	});
    
    	this.add(panel);
    	this.setVisible(true);
    }
    
    
    void updateBText(Integer key1, Integer key2) {
    	this.buttons.get(key1).setText(logics.getValueAt(key1).toString());
    	this.buttons.get(key2).setText(logics.getValueAt(key2).toString());
    }
}
