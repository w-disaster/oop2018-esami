package a03a.e2;

import java.util.HashMap;
import java.util.Map;

public class LogicsImpl implements Logics{

	/* this map contains as key the key position in the matrix, as value the number associated and 
	 * a boolean that indicates if the value can be written (modified)
	 */
	private Map<Pair<Integer, Integer>, Pair<Integer, Boolean>> map;
	public static final int LIMIT = 6;
	
	
	public LogicsImpl() {
		this.map = new HashMap<>();
		for(int i = 0; i < LIMIT; i++) {
			for(int k = 0; k < LIMIT; k++) {
				this.map.put(new Pair<>(i, k), new Pair<>(0, true));
			}
		}
	}

	@Override
	public void hit(Pair<Integer, Integer> key) {
		for(int i = key.getX() - 1; i <= key.getX() + 1; i++) {
			for(int k = key.getY() - 1; k <= key.getY() + 1; k++) {
				if(i >= 0 && i < LIMIT && k >= 0 && k < LIMIT) {
					this.increment(new Pair<>(i, k));
				}
			}
		}
	}

	@Override
	public void increment(Pair<Integer, Integer> key) {
		/* we increment the key value and we set false if the limit is reached */ 
		if(this.map.get(key).getY().equals(true)) {
			Integer newValue = this.map.get(key).getX() + 1;
			this.replaceEntry(key, new Pair<>(this.map.get(key).getX() + 1, newValue.equals(LIMIT - 1) ? false : true));
		}
	}

	@Override
	public void replaceEntry(Pair<Integer, Integer> key, Pair<Integer, Boolean> value) {
		this.map.replace(key, value);
	}

	@Override
	public Boolean isValueEditable(Pair<Integer, Integer> key) {
		return this.map.get(key).getY();
	}

	@Override
	public Integer getValue(Pair<Integer, Integer> key) {
		return this.map.get(key).getX();
	}

	@Override
	public Boolean gameEnded() {
	/* if there is still a 0 value, the game must not end */
		if(this.map.values()
				.stream()
				.filter(v -> v.getX().equals(0))
				.count() > 0) {
			return false;
		}
		return true;
	}

}
