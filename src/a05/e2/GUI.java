package a05.e2;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private Map<JButton, Pair<Integer, Integer>> buttons;
    private Logics logics;
    
    public GUI() {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        
        JPanel panel = new JPanel(new GridLayout(4, 4));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        this.buttons = new HashMap<>();
        this.logics = new LogicsImpl();
        
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                JButton jb = new JButton(" ");
                this.buttons.put(jb, new Pair<>(i, j));
                jb.addActionListener(l -> {
                	if(this.logics.isAdjacent(this.buttons.get(jb))) {
                		jb.setText(this.logics.hit(this.buttons.get(jb)).toString());
                		jb.setEnabled(false);
                	}
                	if(this.logics.mustQuit()) {
                		System.exit(0);
                	}
                });
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }
}
