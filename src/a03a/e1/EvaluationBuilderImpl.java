package a03a.e1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import a03a.e1.Evaluation.*;


public class EvaluationBuilderImpl implements EvaluationBuilder{
	
	// <Corso, <Studente, Risposte>>  
	private Map<String, Map<Integer, Map<Question, Result>>> map;
	boolean alreadyBuilded;

	public EvaluationBuilderImpl() {
		this.map = new HashMap<>();
		this.alreadyBuilded = false;
	}

	@Override
	public EvaluationBuilder addEvaluationByMap(String course, int student, Map<Question, Result> results) {
		/* if the course is already added */
		if(results.isEmpty()) {
			throw new IllegalArgumentException();
		}
		if(this.map.containsKey(course)) {
			/* if the student already committed the evaluation there is an error */
			if(this.map.get(course).containsKey(student)) {
				throw new IllegalStateException();
			}
			/* we add the new student to the course */
			this.map.get(course).put(student, results);
		} else { /* the course is not already present */
			/* we add the course */
			this.map.put(course, new HashMap<>());
			/* we add the results */
			this.map.get(course).put(student, results);
		}
		return this;
	}

	@Override
	public EvaluationBuilder addEvaluationByResults(String course, int student, Result resOverall, Result resInterest,
			Result resClarity) {
		Map<Question, Result> results = new HashMap<>();
		results.put(Question.OVERALL, resOverall);
		results.put(Question.INTEREST, resInterest);
		results.put(Question.CLARITY, resClarity);
		return this.addEvaluationByMap(course, student, results);
	}

	@Override
	public Evaluation build() {
		
		
		if(this.alreadyBuilded) {
			throw new IllegalStateException();
		}
		this.alreadyBuilded = true;
		return new EvaluationImpl();
	}
	
	
	public class EvaluationImpl implements Evaluation {

			@Override
			public Map<Question, Result> results(String course, int student) {
				Map<Question, Result> results = new HashMap<>();
				if(map.containsKey(course)) {
					if(map.get(course).containsKey(student)) {
						results = map.get(course).get(student);
					}
				}
				return results;
			}

			@Override
			public Map<Result, Long> resultsCountForCourseAndQuestion(String course, Question questions) {
				Map<Result, Long> countMap = new HashMap<>();
				if(!map.containsKey(course)) {
					throw new IllegalStateException();
				}
				Iterator<Map<Question, Result>> evaluations = map.get(course).values().iterator();
				while(evaluations.hasNext()) {
					Map<Question, Result> studentEvaluation = evaluations.next();
					studentEvaluation.entrySet().forEach(e -> {
						if(e.getKey().equals(questions)) {
							if(!countMap.containsKey(e.getValue())) {
								countMap.put(e.getValue(), new Long(1));
							} else {
								countMap.replace(e.getValue(), countMap.get(e.getValue()) + 1);
							}
						}
					});
				}
				return countMap;
			}

			@Override
			public Map<Result, Long> resultsCountForStudent(int student) {
				Map<Result, Long> stuCountMap = new HashMap<>();
				map.entrySet().forEach(e -> {
					if(e.getValue().containsKey(student)) {
						Map<Question, Result> studentEvaluation = e.getValue().get(student);
						studentEvaluation.entrySet().forEach(ev -> {
							if(!stuCountMap.containsKey(ev.getValue())) {
								stuCountMap.put(ev.getValue(), Long.valueOf(1));
							} else {
								stuCountMap.replace(ev.getValue(), stuCountMap.get(ev.getValue()) + 1);
							}
						});
					}
				});
				return stuCountMap;
			}

			@Override
			public double coursePositiveResultsRatio(String course, Question question) {
				if(map.containsKey(course)) {
					Double positives = (double) 0;
					List<Result> lres = map.get(course)
							.entrySet()
							.stream()
							.filter(e -> e.getValue().containsKey(question))
							.map(m -> m.getValue().get(question))
							.collect(Collectors.toList());
					for(Result r : lres) {
						positives = positives + (r.equals(Result.WEAKLY_POSITIVE) || r.equals(Result.FULLY_POSITIVE) ? 1 : 0);
					}
					return positives / lres.size();
				} else {
					throw new IllegalStateException();
				}
			}
	}
	
}
