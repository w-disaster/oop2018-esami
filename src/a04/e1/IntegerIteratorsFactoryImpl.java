package a04.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntegerIteratorsFactoryImpl implements IntegerIteratorsFactory{

	@Override
	public SimpleIterator<Integer> empty() {
		return () -> Optional.empty();
	}

	@Override
	public SimpleIterator<Integer> fromList(List<Integer> list) {
		Iterator<Integer> si = list.iterator(); 
		return () -> si.hasNext() ? Optional.of(si.next()) : Optional.empty();
	}

	@Override
	public SimpleIterator<Integer> random(int size) {
		List<Integer> list = Stream.generate(() -> new Random().nextInt(size))
				.limit(size)
				.collect(Collectors.toList());
		return this.fromList(list);
	}

	@Override
	public SimpleIterator<Integer> quadratic() {
		Iterator<Integer> si = Stream.iterate(1, i -> i + 1)
				.flatMap(i -> Stream.iterate(i, n -> n)
				.limit(i))
				.iterator();
		return () -> Optional.of(si.next());
	}

	@Override
	public SimpleIterator<Integer> recurring() {
		Iterator<Integer> si = Stream.iterate(0, i -> i + 1)
				.flatMap(i -> Stream.iterate(0, n -> n + 1)
				.limit(i + 1))
				.iterator();
		return () -> Optional.of(si.next());
	}

	
}
