package a05.e2;

public interface Logics {
	
	public enum State {
		ENABLED, DISABLED;
	}

	Boolean isAdjacent(Pair<Integer, Integer> coordinate);
	
	Integer hit(Pair<Integer, Integer> coordinate);
	
	Boolean mustQuit();
	
}
