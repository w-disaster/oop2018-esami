package a04.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private Map<Pair<Integer, Integer>, JButton> buttons;
	private Logics logics;
	public static final int SIZE = 4;
	
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100, 100);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        this.buttons = new HashMap<>();
        this.logics = new LogicsImpl();
        
        for(int i = 0; i < SIZE; i++) {
			for(int k = 0; k < SIZE; k++) {
				JButton nb = new JButton("");
				this.buttons.put(new Pair<>(i,k), nb);
				panel.add(nb);
			}
		}
        
        this.buttons.entrySet().forEach(e -> {
        	e.getValue().addActionListener(l -> {
        		e.getValue().setEnabled(!logics.hit(e.getKey()));
        		if(logics.end(e.getKey())) {
        			System.exit(0);
        		}
        	});
        });
        
        this.setVisible(true);

    }
}
