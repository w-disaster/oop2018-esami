package a01b.e2;

import java.util.Map;

public interface Logics {

	/**
	 * @param pos
	 * @return true if the button clicked position is the same of the bishop's. false otherwise. 
	 */
	boolean isBishopPosition(Pair<Integer, Integer> pos);
	
	/**
	 * put enabled all the diagonal buttons from pos, disabled all the others.
	 * @param pos
	 */
	void disableNonDiagonal(Pair<Integer, Integer> pos);
	
	/**
	 * if the user clicks the bishop then the next button clicked is his new position.
	 * @return
	 */
	boolean isBishopMoving();
	
	/**
	 * @return true if bishop position is in the origin
	 */
	boolean gameTerminated();
	
	/**
	 * @return the grid map
	 */
	Map<Pair<Integer, Integer>, Boolean> getGrid();
	
	
	/**
	 * 
	 * @param state : true if will be moved, false otherwise
	 */
	void setBishopState(boolean state);
	
	
	/**
	 * @param pos : new bishop position
	 */
	void setBishopPosition(Pair<Integer, Integer> pos); 
}
