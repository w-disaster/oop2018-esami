package a03b.e1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

public class ConferenceReviewingImpl implements ConferenceReviewing{

	private Map<Integer, List<Map<Question, Integer>>> map;
	
	public ConferenceReviewingImpl() {
		this.map = new HashMap<>();
	}

	@Override
	public void loadReview(int article, Map<Question, Integer> scores) {
		/* if the article is already added, we add only the map */
		if(this.map.containsKey(article)){
			this.map.get(article).add(scores);
		} else {
		/* otherwise we put the article and we add the map in the list */
			List<Map<Question, Integer>> lvalues = new ArrayList<>();
			lvalues.add(scores);
			this.map.put(article, lvalues);
		}
	}

	@Override
	public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
		Map<Question, Integer> scores = new HashMap<>();
		scores.put(Question.RELEVANCE, relevance);
		scores.put(Question.SIGNIFICANCE, significance);
		scores.put(Question.CONFIDENCE, confidence);
		scores.put(Question.FINAL, fin);
		this.loadReview(article, scores);
	}

	@Override
	public List<Integer> orderedScores(int article, Question question) {
		if(!this.map.containsKey(article)) {
			throw new IllegalStateException();
		}
		List<Integer> l = new ArrayList<>();
		l.addAll(this.map.get(article)
				.stream()
				.map(m -> m.get(question))
				.sorted((n1, n2) -> n1 - n2)
				.collect(Collectors.toList()));
		return l;
	}

	@Override
	public double averageFinalScore(int article) {
		return this.orderedScores(article, Question.FINAL)
				.stream()
				.mapToDouble(i -> new Double(i))
				.average()
				.getAsDouble();
	}

	@Override
	public Set<Integer> acceptedArticles() {
		return this.map.keySet()
				.stream()
				.filter(i -> this.averageFinalScore(i) > 5)
				.filter(i -> this.map.get(i).stream().filter(m -> m.get(Question.RELEVANCE) > 7).count() > 0)
				.collect(Collectors.toSet());
	}

	@Override
	public List<Pair<Integer, Double>> sortedAcceptedArticles() {
		List<Pair<Integer, Double>> sortedList = new ArrayList<>();
		sortedList.addAll(this.acceptedArticles()
				.stream()
				.map(i -> new Pair<Integer, Double>(i, this.averageFinalScore(i)))
				.sorted((p1, p2) -> Double.compare(p1.getY(), p2.getY()))
				.collect(Collectors.toList()));

		return sortedList;
	}

	@Override
	public Map<Integer, Double> averageWeightedFinalScoreMap() {
		Map<Integer, Double> retMap = new HashMap<>();
		for(Entry<Integer, List<Map<Question, Integer>>> e : this.map.entrySet()) {
			Double pAverage = this.map.get(e.getKey()).stream()
			.mapToDouble(m -> ((double)(m.get(Question.CONFIDENCE) * m.get(Question.FINAL))/10))
			.average()
			.getAsDouble();
			retMap.put(e.getKey(), pAverage);
		}
		return retMap;
	}
	

}
