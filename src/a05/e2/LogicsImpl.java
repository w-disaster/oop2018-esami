package a05.e2;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LogicsImpl implements Logics {

	private Map<Pair<Integer, Integer>, State> coordinates;
	private Optional<Pair<Integer, Integer>> justDisabled;
	private Integer counter;
	
	public LogicsImpl() {
		super();
		this.coordinates = new HashMap<>();
		for(int i = 0; i < 4; i++) {
			for(int k = 0; k < 4; k++) {
				this.coordinates.put(new Pair<>(i, k), State.ENABLED);
			}
		}
		this.justDisabled = Optional.empty();
		this.counter = -1;
	}

	@Override
	public Boolean isAdjacent(Pair<Integer, Integer> coordinate) {
		return this.justDisabled.isEmpty() ? true : 
			this.justDisabled.get().equals(new Pair<Integer, Integer>(coordinate.getX(), coordinate.getY() - 1)) ||
			this.justDisabled.get().equals(new Pair<Integer, Integer>(coordinate.getX(), coordinate.getY() + 1)) ||
			this.justDisabled.get().equals(new Pair<Integer, Integer>(coordinate.getX() - 1, coordinate.getY())) ||
			this.justDisabled.get().equals(new Pair<Integer, Integer>(coordinate.getX() + 1, coordinate.getY()));
	}

	@Override
	public Integer hit(Pair<Integer, Integer> coordinate) {
		this.coordinates.replace(coordinate, State.DISABLED);
		this.justDisabled = Optional.of(coordinate);
		this.counter = this.counter + 1;
		return this.counter;
	}

	@Override
	public Boolean mustQuit() {
		return this.coordinates.entrySet().stream()
				.filter(e -> e.getValue().equals(State.ENABLED))
				.allMatch(e -> !this.isAdjacent(e.getKey()));
	}

}
