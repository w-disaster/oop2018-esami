package a03b.e2;

public interface Logics {
	
	/**
	 * the button with index 'index' has been pushed
	 * @param index
	 */
	void hit(Integer index);
	
	/**
	 * set true or false the direction
	 */
	void switchDirection();
	
	/**
	 * switch two values
	 * @param index1
	 * @param index2
	 */
	void switchValues(Integer index1, Integer index2);

	
	/**
	 * @return true if the values are in ascending order, false otherwise
	 */
	Boolean areAscendingOrdered();
	
	/**
	 * @param index
	 * @return value in the index position
	 */
	public Integer getValueAt(Integer index);
	
	/**
	 * @return true if the direction is right, false otherwise
	 */
	Boolean getDirection();
	
	
}
