package a04.e2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class LogicsImpl implements Logics{

	/* posizione , se è adiacente, se è confrontabile (bottone disabilitato) */
	private Map<Pair<Integer, Integer>, Pair<Boolean, Boolean>> map;
	public static final int SIZE = 4;
	
	public LogicsImpl() {
		this.map = new HashMap<>();
		for(int i = 0; i < SIZE; i++) {
			for(int k = 0; k < SIZE; k++) {
				this.map.put(new Pair<>(i,k), new Pair<>(true, true));
			}
		}
	}

	@Override
	public boolean hit(Pair<Integer, Integer> key) {
		if(this.map.get(key).getX()) {
			this.map.entrySet().forEach(e -> {
				if(e.getValue().getY()) {
					this.map.replace(e.getKey(), new Pair<>(this.isAdjacent(key, e.getKey()), true));
				}
			});
			this.map.replace(key, new Pair<>(true, false));
			return true;
		}
		return false;
	}

	@Override
	public boolean isAdjacent(Pair<Integer, Integer> reference, Pair<Integer, Integer> key) {
		if(this.map.get(key).getY()) {
			return Math.abs(reference.getX() - key.getX()) <= 1 && Math.abs(reference.getY() - key.getY()) <= 1;
		}
		return false;
	}

	@Override
	public boolean end(Pair<Integer, Integer> key) {
		for(Entry<Pair<Integer, Integer>, Pair<Boolean, Boolean>> entry : this.map.entrySet()) {
			if(this.isAdjacent(key, entry.getKey())) {
				return false;
			}
		}
		return true;
	}
}
