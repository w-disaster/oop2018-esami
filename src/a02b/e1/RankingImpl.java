package a02b.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class RankingImpl implements Ranking {

	private List<Tournament> tournaments;
	private Tournament currentTournament;
	
	public RankingImpl() {
		this.tournaments = new ArrayList<>();
		this.currentTournament = null;
	}
	
	@Override
	public void loadTournament(Tournament tournament) {
		if(this.currentTournament != null) {
			if(this.currentTournament.getYear() > tournament.getYear() || 
					(this.currentTournament.getYear() == tournament.getYear() && this.currentTournament.getWeek() >= tournament.getWeek())) {
				throw new IllegalStateException();
			}
		}
		this.tournaments.add(tournament);
		this.currentTournament = tournament;
	}

	@Override
	public int getCurrentWeek() {
		if(this.currentTournament == null) {
			throw new IllegalStateException();
		}
		return this.currentTournament.getWeek();
	}

	@Override
	public int getCurrentYear() {
		if(this.currentTournament == null) {
			throw new IllegalStateException();
		}
		return this.currentTournament.getYear();
	}

	@Override
	public Integer pointsFromPlayer(String player) {
		return this.tournaments.stream()
				.filter(t -> this.currentTournament.getYear() - t.getYear() <=1 )
				.filter(t -> t.getYear() == this.getCurrentYear() - 1 ? t.getWeek() > this.currentTournament.getWeek() : true)
				.map(t -> t.getResult(player))
				.filter(p -> p.isPresent())
				.mapToInt(p -> p.get())
				.sum();
	}

	@Override
	public List<String> ranking() {
		return this.tournaments.stream()
				.filter(t -> this.currentTournament.getYear() - t.getYear() <= 1)
				.flatMap(t -> t.getPlayers().stream())
				.distinct()
				.map(p -> new Pair<>(p, pointsFromPlayer(p)))
				.sorted((p1, p2) -> p2.getY() - p1.getY())
				.map(p -> p.getX())
				.collect(Collectors.toList());
	}

	@Override
	public Map<String, String> winnersFromTournamentInLastYear() {
		return this.tournaments.stream()
				.filter(t -> this.currentTournament.getYear() - t.getYear() <= 1)
				.map(t -> new Pair<String, String>(t.getName(), t.winner()))
				.collect(Collectors.toMap(e -> e.getX(), e -> e.getY()));
	}

	@Override
	public Map<String, Integer> pointsAtEachTournamentFromPlayer(String player) {
		return this.tournaments.stream()
				.map(t -> new Pair<String, Optional<Integer>>(t.getName(), t.getResult(player)))
				.filter(p -> p.getY().isPresent())
				.map(p -> new Pair<String, Integer>(p.getX(), p.getY().get()))
				.collect(Collectors.toMap(p -> p.getX(), p -> p.getY()));
	}

	@Override
	public List<Pair<String, Integer>> pointsAtEachTournamentFromPlayerSorted(String player) {
		return this.pointsAtEachTournamentFromPlayer(player).entrySet().stream()
			.map(e -> new Pair<>(this.getTournamentByName(e.getKey()).get(), e.getValue()))
			.sorted((p1, p2) -> p1.getX().getYear() - p2.getX().getYear())
			.map(p -> new Pair<>(p.getX().getName(), p.getY()))
			.collect(Collectors.toList());
	}
	
	private Optional<Tournament> getTournamentByName(String name) {
		return this.tournaments.stream()
				.filter(t -> t.getName().equals(name))
				.findFirst();
	}

}
