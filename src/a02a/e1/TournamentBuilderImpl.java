package a02a.e1;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Optional;
import java.util.function.Function;

import a02a.e1.Tournament.Result;
import a02a.e1.Tournament.Type;

public class TournamentBuilderImpl implements TournamentBuilder {
	
	private String name;
	private Optional<Type> type;
	private Map<String, Integer> initRankMap;
	private Map<String, Pair<Integer, Result>> resRankMap;
	
	public TournamentBuilderImpl() {
		this.initRankMap = new HashMap<>();
		this.resRankMap = new HashMap<>();
		this.name = "";
		this.type = Optional.empty();
	}

	@Override
	public TournamentBuilder setType(Type type) {
		this.type = Optional.of(type);
		return this;
	}

	@Override
	public TournamentBuilder setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public TournamentBuilder setPriorRanking(Map<String, Integer> ranking) {
		if(this.type.isEmpty()  || this.name.isEmpty()) {
			throw new IllegalStateException();
		}
		this.initRankMap = ranking;
		return this;
	}

	@Override
	public TournamentBuilder addResult(String player, Result result) {
		if(this.initRankMap.isEmpty()) {
			throw new IllegalStateException();
		}
		int amount = 0;
		int initScore = 0;
		
		switch(this.type.get()) {
		case MAJOR:
			amount = 2500;
			break;
		case ATP1000:
			amount = 1000;
			break;
		case ATP500:
			amount = 500;
			break;
		case ATP250:
			amount = 250;
			break;
		}
		
		if(!this.initRankMap.containsKey(player)) {
			this.initRankMap.put(player, 0);
		} 
		initScore = this.initRankMap.get(player);
		
		switch(result) {
		case WINNER: 
			this.resRankMap.put(player, new Pair<>(initScore + amount, result));
			break;
		case FINALIST: 
			this.resRankMap.put(player, new Pair<>(initScore + amount * 50 / 100, result));
			break;
		case SEMIFINALIST:
			this.resRankMap.put(player, new Pair<>(initScore + amount * 20 / 100, result));
			break;
		case QUARTERFINALIST: 
			this.resRankMap.put(player, new Pair<>(initScore + amount * 10 / 100, result));
			break;
		case PARTICIPANT:
			this.resRankMap.put(player, new Pair<>(initScore, result));
			break;
		}
		return this;
	}

	@Override
	public Tournament build() {
		return new TournamentImpl();
	}

	public class TournamentImpl implements Tournament{

		private boolean isAlreadyBuilded;
		
		public TournamentImpl() {
			if(this.isAlreadyBuilded) {
				throw new IllegalStateException();
			}
			this.isAlreadyBuilded = true;
		}

		@Override
		public Type getType() {
			return type.get();
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Optional<Result> getResult(String player) {
			return Optional.ofNullable(resRankMap.containsKey(player) ? resRankMap.get(player).getY() : null);
		}

		@Override
		public String winner() {
			for(Entry<String, Pair<Integer, Result>> entry : resRankMap.entrySet()) {
				if(entry.getValue().getY().equals(Result.WINNER)) {
					return entry.getKey();
				}
			}
			return null;
		}
		@Override
		public Map<String, Integer> initialRanking() {
			return initRankMap;
		}

		@Override
		public Map<String, Integer> resultingRanking() {
			Map<String, Integer> retMap = new HashMap<>();
			initRankMap.entrySet().forEach(ie -> {
				if(!resRankMap.containsKey(ie.getKey())) {
					retMap.put(ie.getKey(), ie.getValue());
				}
			});
			resRankMap.entrySet().forEach(e -> {
				retMap.put(e.getKey(), e.getValue().getX());
			});
			return retMap;
		}

		@Override
		public List<String> rank() {
			return resRankMap.entrySet().stream()
					.sorted((e1, e2) -> e2.getValue().getX() - e1.getValue().getX())
					.map(e -> e.getKey())
					.collect(Collectors.toList());
		}
		
	}
}
