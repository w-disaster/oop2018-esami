package a02b.e1;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class TournamentFactoryImpl implements TournamentFactory {

	@Override
	public Tournament make(String name, int year, int week, Set<String> players, Map<String, Integer> points) {
		return new Tournament() {

			@Override
			public String getName() {
				return name;
			}

			@Override
			public int getYear() {
				return year;
			}

			@Override
			public int getWeek() {
				return week;
			}

			@Override
			public Set<String> getPlayers() {
				return players;
			}

			@Override
			public Optional<Integer> getResult(String player) {
				Optional<Integer> opt = players.contains(player) ? Optional.of(0) : Optional.empty();
				return points.containsKey(player) ? Optional.of(points.get(player)) : opt;
			}

			@Override
			public String winner() {
				return points.entrySet().stream()
						.max((e1, e2) -> e1.getValue() - e2.getValue())
						.map(e -> e.getKey())
						.get();
			}
			
		};
	}

}
