package a03a.e2;

public interface Logics {
	
	/**
	 * pushing the button means increment the index value, and its adjacent buttons
	 * @param index
	 */
	void hit(Pair<Integer, Integer> key);

	/**
	 * increments the value and calls setUneditable
	 * @param index
	 */
	void increment(Pair<Integer, Integer> key);
	
	/**
	 * replaces the entry with key : key
	 * @param index
	 */
	void replaceEntry(Pair<Integer, Integer> key, Pair<Integer, Boolean> value);
	
	/**
	 * @param key
	 * @return if the value is editable
	 */
	Boolean isValueEditable(Pair<Integer, Integer> key);
	
	/**
	 * @param key
	 * @return the key's value
	 */
	Integer getValue(Pair<Integer, Integer> key);
	
	/**
	 * @return true if all the keys have positive values associated
	 */
	Boolean gameEnded();
}
