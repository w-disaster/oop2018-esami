package a03b.e2;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics{

	private List<Integer> values;
	/* true : right, false: left */
	private Boolean direction;
	public static final Integer LIMIT = 5;
	
	public LogicsImpl() {
		this.values = Stream.generate(() -> new Random().nextInt(9))
				.limit(LIMIT)
				.collect(Collectors.toList());
		this.direction = false;
	}

	@Override
	public void hit(Integer index) {
		if(!this.direction && index <= LIMIT) {
			this.switchValues(index, index + 1);
		} else if(this.direction && index >= 0) {
			this.switchValues(index, index - 1);
		}
	}
	
	@Override
	public void switchValues(Integer index1, Integer index2) {
		int tmpNumber = this.values.get(index1);
		this.values.set(index1, this.values.get(index2));
		this.values.set(index2, tmpNumber);
	}
	
	@Override
	public void switchDirection() {
		this.direction = !this.direction;
	}
	

	@Override
	public Boolean areAscendingOrdered() {
		Integer prec = 0;
		for(Integer v : this.values) {
			if(v < prec) {
				return false;
			}
			prec = v;
		}
		
		return true;
	}

	public Integer getValueAt(Integer index) {
		return this.values.get(index);
	}

	@Override
	public Boolean getDirection() {
		return this.direction;
	}



}
