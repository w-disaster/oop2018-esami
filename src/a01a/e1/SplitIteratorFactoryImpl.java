package a01a.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SplitIteratorFactoryImpl implements SplitIteratorFactory{

	private List<Integer> lit;
	
	public SplitIteratorFactoryImpl() {
		lit = Stream.iterate(0, i -> i + 1).limit(100).collect(Collectors.toList());
	}

	@Override
	public SplitIterator<Integer> fromRange(int start, int stop) {
		List<Integer> lrange = this.lit.subList(start, stop + 1);
		
		return new SplitIterator<Integer>() {
			
			private Iterator<Integer> itrange = lrange.iterator();
			
			@Override
			public Optional<Integer> next() {
				return Optional.ofNullable(itrange.hasNext() ? itrange.next() : null);
			}

			@Override
			public SplitIterator<Integer> split() {
				return fromRange(start, stop/2);
			}
		};
	}

	@Override
	public SplitIterator<Integer> fromRangeNoSplit(int start, int stop) {
		List<Integer> lns = this.lit.subList(start, stop);

		return new SplitIterator<Integer>() {
			
			private Iterator<Integer> itrange = lns.iterator();
			
			@Override
			public Optional<Integer> next() {
				return Optional.ofNullable(itrange.hasNext() ? itrange.next() : null);
			}

			@Override
			public SplitIterator<Integer> split() {
				throw new UnsupportedOperationException();
			}
			
		};
	}

	@Override
	public <X> SplitIterator<X> fromList(List<X> list) {
		return new SplitIterator<X>() {

			Iterator<X> listit = list.iterator();
			
			@Override
			public Optional<X> next() {
				return Optional.ofNullable(listit.hasNext() ? listit.next() : null);
			}

			@Override
			public SplitIterator<X> split() {
				return fromList(list.subList(list.size()/2, list.size()));
			}
			
		};
	}

	@Override
	public <X> SplitIterator<X> fromListNoSplit(List<X> list) {
		return new SplitIterator<X>() {

			Iterator<X> llns = list.iterator();
			
			@Override
			public Optional<X> next() {
				return Optional.ofNullable(llns.hasNext() ? llns.next() : null);
			}

			@Override
			public SplitIterator<X> split() {
				throw new UnsupportedOperationException();
			}
			
		};
	}

	@Override
	public <X> SplitIterator<X> excludeFirst(SplitIterator<X> si) {
		si.next();
		return new SplitIterator<X>() {

			@Override
			public Optional<X> next() {
				return si.next();
			}

			@Override
			public SplitIterator<X> split() {
				return si.split();
			}
			
		};
	}

	@Override
	public <X, Y> SplitIterator<Y> map(SplitIterator<X> si, Function<X, Y> mapper) {
		return new SplitIterator<Y>() {

			@Override
			public Optional<Y> next() {
				Optional<X> next = si.next();
				return Optional.ofNullable(next.isEmpty() ? null : mapper.apply(next.get()));
			}

			@Override
			public SplitIterator<Y> split() {
				return map(si.split(), mapper);
			}
			
		};
	}

}
