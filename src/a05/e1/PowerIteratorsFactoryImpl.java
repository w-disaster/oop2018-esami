package a05.e1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

	private static <X> PowerIterator<X> powerIteratorFromStream(Stream<X> stream) {
		return new PowerIterator<X>() {

			private Iterator<X> it = stream.iterator();
			private List<X> alreadyProducesElements = new ArrayList<>();
			
			@Override
			public Optional<X> next() {
				Optional<X> next = it.hasNext() ? Optional.of(it.next()) : Optional.empty();
				if(next.isPresent()) {
					this.alreadyProducesElements.add(next.get());
				}
				return next;
			}

			@Override
			public List<X> allSoFar() {
				return this.alreadyProducesElements;
			}

			@Override
			public PowerIterator<X> reversed() {
				return powerIteratorFromStream(Stream.iterate(this.alreadyProducesElements.size() - 1, i -> i - 1)
						.limit(this.alreadyProducesElements.size())
						.map(i -> this.alreadyProducesElements.get(i)));
			}
			
		};
	}
	
	
	@Override
	public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
		return powerIteratorFromStream(Stream.iterate(start, i -> successive.apply(i)));
	}

	@Override
	public <X> PowerIterator<X> fromList(List<X> list) {
		return powerIteratorFromStream(list.stream());
	}

	@Override
	public PowerIterator<Boolean> randomBooleans(int size) {
		return powerIteratorFromStream(Stream.generate(() -> new Random().nextBoolean()).limit(size));
	}

}
