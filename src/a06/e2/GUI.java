package a06.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.stream.*;
import javax.swing.*;

import java.util.ArrayList;
import java.util.List;


public class GUI extends JFrame{
	
	private Logics logics;
	private List<JButton> buttons;
	private JButton reset;
	
	public GUI(int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		this.logics = new LogicsImpl(size);
		this.buttons = new ArrayList<>();
		JPanel panel = new JPanel();
		this.getContentPane().add(BorderLayout.CENTER, panel);
		
		this.buttons = Stream.generate(() -> new JButton("1"))
				.limit(size)
				.collect(Collectors.toList());
		
		this.buttons.forEach(b -> {
			panel.add(b);
			b.addActionListener(l -> {
				b.setText(logics.hit(this.buttons.indexOf(b)).toString());
				b.setEnabled(false);
			});
		});
		
		this.reset = new JButton("Reset");
		panel.add(reset);
		reset.addActionListener(l -> {
			this.logics = new LogicsImpl(size);
			this.buttons.forEach(b -> {
				b.setEnabled(true);
				b.setText("1");
			});
		});
	
		this.setVisible(true);
	}
	
}
