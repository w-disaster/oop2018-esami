package a06.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserFactoryImpl implements ParserFactory {

	private static Parser fromList(List<String> list) {
		return new Parser() {

			private Iterator<String> it = list.stream().iterator();
			
			@Override
			public boolean acceptToken(String token) {
				return !inputCompleted() && it.next().equals(token);
			}

			@Override
			public boolean inputCompleted() {
				return !it.hasNext();
			}

			@Override
			public void reset() {
				this.it = list.stream().iterator();
			}
			
		};
	}
	
	
	@Override
	public Parser one(String token) {
		return this.many(token, 1);
	}

	@Override
	public Parser many(String token, int elemCount) {
		return fromList(Stream.generate(() -> token).limit(elemCount).collect(Collectors.toList()));
	}

	@Override
	public Parser oneOf(Set<String> set) {
		return new Parser() {

			private Optional<String> element = Optional.empty();
			
			@Override
			public boolean acceptToken(String token) {
				if(!this.inputCompleted() && set.contains(token)){
					this.element = Optional.of(token);
					return true;
				}
				return false;
			}

			@Override
			public boolean inputCompleted() {
				return element.isPresent();
			}

			@Override
			public void reset() {
				this.element = Optional.empty();
			}
			
		};
	}

	@Override
	public Parser sequence(String token1, String token2) {
		return fromList(Stream.of(token1, token2).collect(Collectors.toList()));
	}

	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		return new Parser() {
			
			private Iterator<String> it = Stream.generate(() -> elem.stream()).flatMap(s -> s)
					.limit(elemCount)
					.flatMap(s -> Stream.of(s, separator))
					.limit(elemCount*2 - 1)
					.iterator();
			private Optional<String> be = Optional.empty();
			private boolean flag = false;
			
			@Override
			public boolean acceptToken(String token) {
				if(!inputCompleted()) {
					if(this.be.isPresent()) {
						if(this.be.get().equals(begin) && !token.equals(begin) && !token.equals(end)) {
							flag = true;
							return it.hasNext() ? it.next().equals(token) : false;
						}
					}
					if(token.equals(begin) || (token.equals(end) && flag == true)) {
						this.be = Optional.of(token);
						return true;
					}
				}
				return false;
			}

			@Override
			public boolean inputCompleted() {
				return this.be.isPresent() ? this.be.get().equals(end) : false;
			}

			@Override
			public void reset() {
				this.it = elem.stream()
						.limit(elemCount)
						.flatMap(s -> Stream.of(s, separator))
						.limit(elemCount*2 - 1)
						.iterator();
				this.be = Optional.empty();
				this.flag = false;
			}
			
		};
	}

}
