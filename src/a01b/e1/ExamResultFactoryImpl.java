package a01b.e1;

import java.util.Optional;

import a01b.e1.ExamResult.Kind;

public class ExamResultFactoryImpl implements ExamResultFactory{
	
	@Override
	public ExamResult failed() {
		return new ExamResultImpl(Kind.FAILED, Optional.empty());
	}

	@Override
	public ExamResult retired() {
		return new ExamResultImpl(Kind.RETIRED, Optional.empty());
	}

	@Override
	public ExamResult succeededCumLaude() {
		return new ExamResultImpl(Kind.SUCCEEDED, Optional.of(30));
	}

	@Override
	public ExamResult succeeded(int evaluation) {
		return new ExamResultImpl(Kind.SUCCEEDED, Optional.of(evaluation));
	}

	
	public class ExamResultImpl implements ExamResult {

		private Kind kind;
		private Optional<Integer> evaluation;
		
		public ExamResultImpl(Kind kind, Optional<Integer> evaluation) {
			if(evaluation.isPresent()) {
				if(evaluation.get() < 18 || evaluation.get() > 30) {
					throw new IllegalArgumentException();
				}
			}
			this.kind = kind;
			this.evaluation = evaluation;
		}

		@Override
		public Kind getKind() {
			return this.kind;
		}

		@Override
		public Optional<Integer> getEvaluation() {
			return this.evaluation;
		}

		@Override
		public boolean cumLaude() {
			return this.evaluation.isPresent() ? this.evaluation.get().equals(30) : false;
		}

		@Override
		public String toString() {
			return this.getEvaluation().isEmpty() ? this.kind.name() : this.kind.name() + "(" + this.getEvaluation().get() + 
					(this.getEvaluation().get().equals(30) ? "L" : "")  + ")";
		}

	}
}
