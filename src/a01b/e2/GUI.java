package a01b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
    
    public GUI() {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300,300);
        
        Logics logics = new LogicsImpl();
        JPanel panel = new JPanel(new GridLayout(5,5));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        Map<JButton, Pair<Integer, Integer>> bMap = new HashMap<>();
        for(int x = 0; x < 5; x++) {
        	for(int y = 0; y < 5; y++) {
        		JButton button = logics.isBishopPosition(new Pair<>(x, y)) ? new JButton("b") : new JButton("");
        		bMap.put(button, new Pair<>(x, y));
        		panel.add(button);
        		button.addActionListener(l -> {
        			Pair<Integer, Integer> position = bMap.get(button);
        			if(logics.isBishopPosition(position)) {
        				logics.setBishopState(true);
        				logics.disableNonDiagonal(position);
        				bMap.entrySet()
        				.stream()
        				.forEach(e -> e.getKey().setEnabled(logics.getGrid().get(e.getValue())));
        			} else if(logics.isBishopMoving()){
        				logics.setBishopPosition(bMap.get(button));
        				if(logics.gameTerminated()) {
        					System.exit(0);
        				};
        				logics.setBishopState(false);
        				bMap.keySet().forEach(b -> {
        					if(b.getText().equals("b")) {
        						b.setText("");
        					}
        					b.setEnabled(true);
        				});
        				button.setText("b");
        			}
        			
        		});	
        	}
        }
    
    this.add(panel);
    this.setVisible(true);
    }
    
    
}
