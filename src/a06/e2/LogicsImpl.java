package a06.e2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics{

	private List<Integer> list;
	
	public LogicsImpl(int n) {
		this.list = Stream.generate(() -> 1)
				.limit(n)
				.collect(Collectors.toList());
	}

	@Override
	public Integer hit(Integer index) {
		int value = 0;
		for(int i = 0; i <= index; i++) {
			value = value + this.list.get(i);
		}
		this.list.set(index, value);
		return this.list.get(index);
	}

}
